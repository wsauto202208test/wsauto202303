package co.com.automationtesting.e01_abrirpagina;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static co.com.automationtesting.Utilidades.esperar;
import static org.junit.Assert.assertEquals;

public class AbrirPaginaWDM {
    private WebDriver driver;
    private String url;

    @Before
    public void preparaTest(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        url = "https://parabank.parasoft.com/parabank/index.htm";
    }

    @Test
    public void testAbrirPagina() throws InterruptedException {
        driver.manage().window().maximize();
        driver.get(url);
        esperar(1);
        System.out.println("El titulo a verificar de la pantalla es: "+ driver.findElement(By.xpath("//*[@id=\"topPanel\"]/p")).getText());
        assertEquals("Experience the difference",driver.findElement(By.xpath("//*[@id=\"topPanel\"]/p")).getText());
        esperar(2);
    }


    @After
    public void finalizeAbrirPagina(){
        driver.quit();
    }
}
