package co.com.automationtesting.e07_alertas;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static co.com.automationtesting.Constantes.URL_AUTOMATION_DEMO_ALERTAS;
import static co.com.automationtesting.Utilidades.esperar;

public class Alertas {
    private WebDriver driver;

    @Before
    public void setup(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void alertaWithOk() throws Exception {
        driver.get(URL_AUTOMATION_DEMO_ALERTAS);
        driver.findElement(By.xpath("//button[@onclick='alertbox()']")).click();

        Alert alerta = driver.switchTo().alert();
        System.out.println(alerta.getText());
        esperar(3);
        alerta.accept();
        esperar(3);
    }

    @Test
    public void alertaWithOkCancel() throws Exception {
        driver.get(URL_AUTOMATION_DEMO_ALERTAS);
        driver.findElement(By.xpath("//a[@href='#CancelTab']")).click();
        esperar(2);
        driver.findElement(By.xpath("//button[@onclick='confirmbox()']")).click();

        Alert alerta = driver.switchTo().alert();
        System.out.println(alerta.getText());
        esperar(3);
        alerta.accept();
        esperar(3);
        Assert.assertEquals("You pressed Ok",driver.findElement(By.id("demo")).getText());

        esperar(2);

        driver.findElement(By.xpath("//button[@onclick='confirmbox()']")).click();
        alerta = driver.switchTo().alert();
        esperar(2);
        alerta.dismiss();
        esperar(2);
        Assert.assertEquals("You Pressed Cancel",driver.findElement(By.id("demo")).getText());

        esperar(2);
    }

    @Test
    public void alertaWithTextBox() throws Exception {
        driver.get(URL_AUTOMATION_DEMO_ALERTAS);
        driver.findElement(By.xpath("//a[@href='#Textbox']")).click();
        esperar(2);
        driver.findElement(By.xpath("//button[@onclick='promptbox()']")).click();

        Alert alerta = driver.switchTo().alert();
        System.out.println(alerta.getText());
        alerta.sendKeys("Hola Mundo");
        esperar(3);
        alerta.accept();
        esperar(3);
        Assert.assertEquals("Hello Hola Mundo How are you today",driver.findElement(By.id("demo1")).getText());
        esperar(3);
    }

    @After
    public void close(){
        driver.quit();
    }
}
