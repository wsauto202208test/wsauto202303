package co.com.automationtesting.e05_iframe;

import co.com.automationtesting.Constantes;
import co.com.automationtesting.Utilidades;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static co.com.automationtesting.Constantes.*;
import static co.com.automationtesting.Utilidades.*;

public class Frames {
    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testSingleFrame() throws InterruptedException {
        driver.get(URL_AUTOMATION_DEMO_REGISTER);
        driver.findElement(By.xpath(MENU_SWITCH_TO)).click();
        driver.findElement(By.xpath(MENU_FRAME)).click();
        esperar(3);
        driver.switchTo().frame("aswift_2");
        esperar(2);
        driver.switchTo().frame("ad_iframe");
        esperar(2);
        driver.findElement(By.id("dismiss-button")).click();
        esperar(2);
        driver.switchTo().frame("singleframe");
        driver.findElement(By.xpath(XPATH_ONE_FRAME)).sendKeys(SINGLE_FRAME);
        esperar(2);
        driver.switchTo().defaultContent();
        esperar(2);
        driver.findElement(By.xpath(MENU_HOME)).click();
        esperar(3);

    }

    @Test
    public void testIframeWithInAnIframe() throws InterruptedException {
        driver.get(URL_AUTOMATION_DEMO_REGISTER);
        driver.findElement(By.xpath(MENU_SWITCH_TO)).click();
        driver.findElement(By.xpath(MENU_FRAME)).click();
        esperar(3);

        driver.switchTo().frame("aswift_2");
        esperar(2);
        driver.switchTo().frame("ad_iframe");
        esperar(2);
        driver.findElement(By.id("dismiss-button")).click();
        esperar(2);


        driver.findElement(By.xpath("/html/body/section/div[1]/div/div/div/div[1]/div/ul/li[2]/a")).click();
        esperar(2);
        WebElement primerFrame = driver.findElement(By.xpath("//iframe[@src='MultipleFrames.html']"));
        driver.switchTo().frame(primerFrame);
        esperar(2);
        WebElement segundoFrame = driver.findElement(By.xpath("/html/body/section/div/div/iframe"));
        driver.switchTo().frame(segundoFrame);
        esperar(2);
        //scrollToObject("/html/body/section/div/div/div/input", driver);
        driver.findElement(By.xpath("/html/body/section/div/div/div/input")).sendKeys("TestIframeWithInAnIframe");
        driver.switchTo().defaultContent();

        Assert.assertEquals("Home", driver.findElement(By.xpath("//*[@id='header']/nav/div/div[2]/ul/li[1]/a")).getText());
    }

    @After
    public void close() throws Exception {
        driver.quit();
    }
}
