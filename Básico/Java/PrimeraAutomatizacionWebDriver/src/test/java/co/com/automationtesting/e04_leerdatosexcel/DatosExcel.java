package co.com.automationtesting.e04_leerdatosexcel;

import co.com.automationtesting.Constantes;
import co.com.automationtesting.Utilidades;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static co.com.automationtesting.Constantes.*;
import static co.com.automationtesting.Utilidades.*;
import static org.junit.Assert.*;

public class DatosExcel {
    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void test() throws IOException, InterruptedException {
        String rutaArchivo = RUTA_ARCHIVO;
        FileInputStream archivo = new FileInputStream(rutaArchivo);
        Workbook libro = new XSSFWorkbook(archivo); //Toma el libro del archivo
        Sheet hoja = libro.getSheetAt(0); //Partimos de la primera hoja de mi archivo de excel
        int numeroFilas = hoja.getLastRowNum() - hoja.getFirstRowNum();

        System.out.println("El numero de filas: " + numeroFilas);

        driver.manage().window().maximize();
        driver.get(URL_PARABANK);
        for (int i = 0; i < numeroFilas ; i++) {
            Row fila = hoja.getRow(i+1);
            driver.findElement(By.linkText(LINK_REGISTER)).click();
            driver.findElement(By.id(ID_FIRST_NAME)).sendKeys(fila.getCell(0).getStringCellValue());
            driver.findElement(By.id(ID_LAST_NAME)).sendKeys(fila.getCell(1).getStringCellValue());
            driver.findElement(By.id(ID_ADDRESS)).sendKeys(fila.getCell(2).getStringCellValue());
            driver.findElement(By.id(ID_CITY)).sendKeys(fila.getCell(3).getStringCellValue());
            driver.findElement(By.id(ID_STATE)).sendKeys(fila.getCell(4).getStringCellValue());
            driver.findElement(By.id(ID_ZIP)).sendKeys(fila.getCell(5).getStringCellValue());
            driver.findElement(By.id(ID_PHONE)).sendKeys(fila.getCell(6).getStringCellValue());
            driver.findElement(By.id(ID_SSN)).sendKeys(fila.getCell(7).getStringCellValue());
            driver.findElement(By.id(ID_USER)).sendKeys(fila.getCell(8).getStringCellValue());
            driver.findElement(By.id(ID_PASSWORD)).sendKeys(fila.getCell(9).getStringCellValue());
            driver.findElement(By.id(ID_CONFIRM)).sendKeys(fila.getCell(10).getStringCellValue());
            driver.findElement(By.xpath(BTN_REGISTER)).click();

            assertEquals("Welcome "+fila.getCell(8).getStringCellValue(),driver.findElement(By.xpath(XPATH_REGISTRO_EXITOSO)).getText());
            esperar(2);
            driver.findElement(By.linkText("Log Out")).click();
            esperar(2);
        }
        archivo.close();
    }

    @After
    public void close(){
        driver.quit();
    }
}
