package co.com.automationtesting;

public class Constantes {
    public static final String URL_PARABANK = "https://parabank.parasoft.com/parabank/index.htm";
    public static final String URL_AUTOMATION_DEMO_REGISTER = "https://demo.automationtesting.in/Register.html";
    public static final String URL_AUTOMATION_DEMO_ALERTAS = "https://demo.automationtesting.in/Alerts.html";
    public static final String URL_AUTOMATION_DEMO_VENTANAS = "https://demo.automationtesting.in/Windows.html";
    public static final String URL_TABLAS = "https://demo.guru99.com/test/web-table-element.php";
    public static final String MENU_SWITCH_TO = "//*[@id='header']/nav/div/div[2]/ul/li[4]/a";
    public static final String MENU_FRAME = "//*[@id='header']/nav/div/div[2]/ul/li[4]/ul/li[3]/a";
    public static final String MENU_HOME = "//*[@id='header']/nav/div/div[2]/ul/li[1]/a";

    public static final String RUTA_ARCHIVO = "src/test/resources/dataDriven/datosRegistro.xlsx";
    public static final String LINK_REGISTER = "Register";
    public static final String NOMBRE = "Yeison";
    public static final String APELLIDO = "Arias";
    public static final String ADDRESS = "CL 21 # 12 13";
    public static final String CITY = "Medellin";
    public static final String STATE = "Antioquia";
    public static final String ZIP_CODE = "0500031";
    public static final String PHONE_NUMBER = "3013686868";
    public static final String SSN = "3686868";
    public static final String USER_NAME = "yfarias3";
    public static final String PASSWORD = "yfarias*";
    public static final String REPEATED_PASSWORD = "yfarias*";
    public static final String BTN_REGISTER = "//*[@id='customerForm']/table/tbody/tr[13]/td[2]/input";
    public static final String MENSAJE_REGISTRO_EXITOSO = "//*[@id='customerForm']/table/tbody/tr[13]/td[2]/input";
    public static final String XPATH_REGISTRO_EXITOSO = "//*[@id='rightPanel']/h1";
    public static final String XPATH_REGISTRO_EXISTE = "//*[@id='customerForm']/table/tbody/tr[10]/td[3]";
    public static final String XPATH_ONE_FRAME = "/html/body/section/div/div/div/input";
    public static final String ID_FIRST_NAME = "customer.firstName";
    public static final String ID_LAST_NAME = "customer.lastName";
    public static final String ID_ADDRESS = "customer.address.street";
    public static final String ID_CITY = "customer.address.city";
    public static final String ID_STATE = "customer.address.state";
    public static final String ID_ZIP = "customer.address.zipCode";
    public static final String ID_PHONE = "customer.phoneNumber";
    public static final String ID_SSN = "customer.ssn";
    public static final String ID_USER = "customer.username";
    public static final String ID_PASSWORD = "customer.password";
    public static final String ID_CONFIRM = "repeatedPassword";
    public static final Integer ESPERA_CORTA = 3;
    public static final Integer ESPERA_MEDIA = 5;
    public static final Integer ESPERA_LARGA = 10;
    public static final String SINGLE_FRAME = "Single frame";


}
