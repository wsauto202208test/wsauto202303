package co.com.automationtesting.e03_login;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static co.com.automationtesting.Constantes.*;
import static co.com.automationtesting.Utilidades.esperar;
import static org.junit.Assert.assertEquals;

public class LoginParaBank {
    private static WebDriver driver;

    @Before
    public void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testRegistroExitoso() throws InterruptedException {
        driver.get(URL_PARABANK);
        driver.findElement(By.name("username")).sendKeys("yfarias");
        driver.findElement(By.name("password")).sendKeys("yfarias*");
        driver.findElement(By.xpath("//*[@id=\"loginPanel\"]/form/div[3]/input")).click();

        assertEquals("Welcome " + NOMBRE + " " + APELLIDO, driver.findElement(By.xpath("//*[@id=\"leftPanel\"]/p")).getText());
        esperar(5);
    }



    @After
    public void tearDown() {
        driver.quit();
    }

}
