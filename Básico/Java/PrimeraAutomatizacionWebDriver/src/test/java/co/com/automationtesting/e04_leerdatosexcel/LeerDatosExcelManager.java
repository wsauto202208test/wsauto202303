package co.com.automationtesting.e04_leerdatosexcel;

import co.com.automationtesting.ExcelManager;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileInputStream;
import java.io.IOException;

import static co.com.automationtesting.Constantes.*;
import static co.com.automationtesting.Constantes.XPATH_REGISTRO_EXITOSO;
import static co.com.automationtesting.Utilidades.esperar;
import static org.junit.Assert.assertEquals;

public class LeerDatosExcelManager {
    private WebDriver driver;
    private ExcelManager excelManager;

    @Before
    public void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        excelManager = new ExcelManager();
    }

    @Test
    public void test() throws FilloException, InterruptedException {
        excelManager.strRutaArchivo("C:\\WS JAVA\\202303\\Básico\\Java\\PrimeraAutomatizacionWebDriver\\src\\test\\resources\\datadriven\\datosRegistro.xlsx");
        String strSql = "SELECT * FROM Hoja1";
        Recordset recordset = excelManager.leerExcel(strSql);
        System.out.println("La cantidad de registros en el archivo es: "+ recordset.getCount());

        driver.manage().window().maximize();
        driver.get(URL_PARABANK);

        while(recordset.next()){
            try {
                driver.findElement(By.linkText(LINK_REGISTER)).click();
                driver.findElement(By.id(ID_FIRST_NAME)).sendKeys(recordset.getField("firstname"));
                driver.findElement(By.id(ID_LAST_NAME)).sendKeys(recordset.getField("lastname"));
                driver.findElement(By.id(ID_ADDRESS)).sendKeys(recordset.getField("address"));
                driver.findElement(By.id(ID_CITY)).sendKeys(recordset.getField("city"));
                driver.findElement(By.id(ID_STATE)).sendKeys(recordset.getField("state"));
                driver.findElement(By.id(ID_ZIP)).sendKeys(recordset.getField("zipcode"));
                driver.findElement(By.id(ID_PHONE)).sendKeys(recordset.getField("phone"));
                driver.findElement(By.id(ID_SSN)).sendKeys(recordset.getField("ssn"));
                driver.findElement(By.id(ID_USER)).sendKeys(recordset.getField("user"));
                driver.findElement(By.id(ID_PASSWORD)).sendKeys(recordset.getField("password"));
                driver.findElement(By.id(ID_CONFIRM)).sendKeys(recordset.getField("confirm"));
                driver.findElement(By.xpath(BTN_REGISTER)).click();

                assertEquals("Welcome " + recordset.getField("user"), driver.findElement(By.xpath(XPATH_REGISTRO_EXITOSO)).getText());
                esperar(2);

                if(!driver.findElements(By.xpath(XPATH_REGISTRO_EXITOSO)).isEmpty()) {
                    strSql = "UPDATE Hoja1 SET estado='Registro Exitoso para el usuario: " + recordset.getField("user") + "' WHERE id=" +  Integer.parseInt(recordset.getField("id"));
                    excelManager.ModificarRegistrosExcel(strSql);
                    driver.findElement(By.linkText("Log Out")).click();
                }else{
                    strSql = "UPDATE Hoja1 SET estado='Error: No se registro el usuario: "+recordset.getField("user")+"' WHERE id=" +  Integer.parseInt(recordset.getField("id"));
                    excelManager.ModificarRegistrosExcel(strSql);
                }

            }catch (Exception e){
                strSql = "UPDATE Hoja1 SET estado='Error: No se registro el usuario: "+recordset.getField("user")+"' WHERE id=" +  Integer.parseInt(recordset.getField("id"));
                excelManager.ModificarRegistrosExcel(strSql);
            }
            esperar(2);
        }

    }

    @After
    public void close(){
        driver.quit();
    }
}
