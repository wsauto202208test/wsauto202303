package co.com.automationtesting.e06_recorridodinamico;

import co.com.automationtesting.Constantes;
import co.com.automationtesting.Utilidades;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static co.com.automationtesting.Constantes.*;
import static co.com.automationtesting.Utilidades.*;

public class RecorridoDinamico {
    private WebDriver driver;

    @Before
    public void setup(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testRecorridoRadioButton() throws Exception {
        driver.get(URL_AUTOMATION_DEMO_REGISTER);

        List<WebElement> genero = driver.findElements(
                By.xpath("//*[@id='basicBootstrapForm']/div[5]/div/label"));
        System.out.println("La cantidad de objetos es: "+genero.size());
        String seleccionarGenero = "Male";
        for (int i = 0; i < genero.size(); i++) {
            if (genero.get(i).getText().equals("Male")){
                genero.get(i).click();
            }
        }

        esperar(3);
    }

    @Test
    public void testRecorridoCheckbox() throws Exception {
        driver.get(URL_AUTOMATION_DEMO_REGISTER);

        List<WebElement> hobbies = driver.findElements(By.xpath("//*[@id='basicBootstrapForm']/div[6]/div/div/label"));

        System.out.println("La cantidad de objetos es: "+hobbies.size());
        String seleccionarHobbie = "Hockey,Cricket";

        List<String> listaOpciones = new ArrayList<>();
        listaOpciones.addAll(Arrays.asList(seleccionarHobbie.split(",")));

        for (int j = 0; j < listaOpciones.size(); j++) {
            for (int i = 0; i < hobbies.size(); i++) {
                if (hobbies.get(i).getText().trim().equals(listaOpciones.get(j).trim())){
                    driver.findElement(By.id("checkbox"+(Integer.toString(i+1))+"")).click();
                }
            }
        }

        esperar(5);
    }


    @After
    public void close(){
        driver.quit();
    }
}
