package co.com.automationtesting.e08_ventanas;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static co.com.automationtesting.Constantes.URL_AUTOMATION_DEMO_VENTANAS;
import static co.com.automationtesting.Utilidades.esperar;

public class Ventanas {
    private WebDriver driver;

    @Before
    public void setup(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void openNewTabWindow() throws Exception {
        driver.get(URL_AUTOMATION_DEMO_VENTANAS);
        System.out.println(driver.getWindowHandle());

        String ventanaInicial = driver.getWindowHandle();

        driver.findElement(By.xpath("//a/button[@class='btn btn-info']")).click();

        for (String manejadorVentana : driver.getWindowHandles()) {
            if(!ventanaInicial.contentEquals(manejadorVentana)){
                driver.switchTo().window(manejadorVentana);
            }
        }
        driver.findElement(By.xpath("//*[@id='announcement-banner']/div/div/div/h4/a")).click();

        esperar(3);
    }

    @Test
    public void separateMultipleWindows() throws Exception {
        driver.get(URL_AUTOMATION_DEMO_VENTANAS);
        driver.findElement(By.xpath("//a[@href='#Multiple']")).click();
        driver.findElement(By.xpath("//button[@onclick='multiwindow()']")).click();
        esperar(3);

        int contadorVentanas = 1;
        for (String manejadorVentana : driver.getWindowHandles()) {
            driver.switchTo().window(manejadorVentana);
            System.out.println("El Título de la ventana "+ contadorVentanas +" es: "+ driver.getTitle());
            contadorVentanas++;
        }
        esperar(3);
    }

    @After
    public void close(){
        driver.quit();
    }
}
