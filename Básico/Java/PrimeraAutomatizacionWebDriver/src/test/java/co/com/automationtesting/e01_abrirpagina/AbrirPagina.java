package co.com.automationtesting.e01_abrirpagina;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static co.com.automationtesting.Utilidades.*;
import static org.junit.Assert.*;

public class AbrirPagina {
    private WebDriver driver;
    private String url;

    @Before
    public void preparaTest(){
        //Adicionamos la dependencia de WebDriverManager en el build.gradle y colocamo la siguiente linea
        WebDriverManager.chromedriver().setup();
        //System.setProperty("webdriver.chrome.driver","./src/test/resources/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        url = "https://parabank.parasoft.com/parabank/index.htm";
    }

    @Test
    public void testAbrirPagina() throws InterruptedException {
        driver.manage().window().maximize();
        driver.get(url);
        esperar(1);
        System.out.println("El titulo a verificar de la pantalla es: "+ driver.findElement(By.xpath("//*[@id=\"topPanel\"]/p")).getText());
        assertEquals("Experience the difference",driver.findElement(By.xpath("//*[@id=\"topPanel\"]/p")).getText());
        esperar(2);

    }


    @After
    public void finalizeAbrirPagina(){
        driver.quit();
    }
}
