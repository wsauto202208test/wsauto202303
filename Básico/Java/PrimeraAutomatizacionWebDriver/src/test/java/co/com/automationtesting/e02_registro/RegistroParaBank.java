package co.com.automationtesting.e02_registro;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static co.com.automationtesting.Constantes.*;
import static co.com.automationtesting.Utilidades.*;
import static org.junit.Assert.assertEquals;

public class RegistroParaBank {
    private static WebDriver driver;

    @Before
    public void setUp() throws Exception {
        //System.setProperty("webdriver.chrome.driver","./src/test/resources/drivers/chromedriver.exe");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testRegistroExitoso() throws InterruptedException {
        driver.get(URL_PARABANK);
        esperar(2);
        scrollToObjectXpath("//*[@id='footerPanel']", driver);
        esperar(3);
        driver.findElement(By.linkText(LINK_REGISTER)).click();
        esperar(ESPERA_CORTA);
        driver.findElement(By.id("customer.firstName")).sendKeys(NOMBRE);
        driver.findElement(By.id("customer.lastName")).sendKeys(APELLIDO);
        driver.findElement(By.id("customer.address.street")).sendKeys(ADDRESS);
        driver.findElement(By.id("customer.address.city")).sendKeys(CITY);
        driver.findElement(By.id("customer.address.state")).sendKeys(STATE);
        driver.findElement(By.id("customer.address.zipCode")).sendKeys(ZIP_CODE);
        driver.findElement(By.id("customer.phoneNumber")).sendKeys(PHONE_NUMBER);
        driver.findElement(By.id("customer.ssn")).sendKeys(SSN);
        driver.findElement(By.id("customer.username")).sendKeys(USER_NAME);
        driver.findElement(By.id("customer.password")).sendKeys(PASSWORD);
        driver.findElement(By.id("repeatedPassword")).sendKeys(REPEATED_PASSWORD);
        esperar(2);
        driver.findElement(By.xpath(BTN_REGISTER)).click();

        assertEquals("Welcome "+USER_NAME, driver.findElement(By.xpath(XPATH_REGISTRO_EXITOSO)).getText());
        esperar(10);
    }

    @Test
    public void testExistenteFallido() throws Exception {
        driver.get(URL_PARABANK);
        driver.findElement(By.linkText(LINK_REGISTER)).click();
        esperar(ESPERA_CORTA);
        driver.findElement(By.id("customer.firstName")).sendKeys(NOMBRE);
        driver.findElement(By.id("customer.lastName")).sendKeys(APELLIDO);
        driver.findElement(By.id("customer.address.street")).sendKeys(ADDRESS);
        driver.findElement(By.id("customer.address.city")).sendKeys(CITY);
        driver.findElement(By.id("customer.address.state")).sendKeys(STATE);
        driver.findElement(By.id("customer.address.zipCode")).sendKeys(ZIP_CODE);
        driver.findElement(By.id("customer.phoneNumber")).sendKeys(PHONE_NUMBER);
        driver.findElement(By.id("customer.ssn")).sendKeys(SSN);
        driver.findElement(By.id("customer.username")).sendKeys("yfarias1");
        driver.findElement(By.id("customer.password")).sendKeys(PASSWORD);
        driver.findElement(By.id("repeatedPassword")).sendKeys(REPEATED_PASSWORD);
        driver.findElement(By.xpath(BTN_REGISTER)).click();

        assertEquals("This username already exists.", driver.findElement(By.xpath(XPATH_REGISTRO_EXISTE)).getText());
        esperar(4);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
