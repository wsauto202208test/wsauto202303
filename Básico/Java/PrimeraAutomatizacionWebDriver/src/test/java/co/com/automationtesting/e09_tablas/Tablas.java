package co.com.automationtesting.e09_tablas;

import co.com.automationtesting.Utilidades;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import static co.com.automationtesting.Constantes.URL_TABLAS;

public class Tablas {
    private WebDriver driver;

    @Before
    public void setup(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void validarCantidadFilasColumas() throws Exception {
        driver.get(URL_TABLAS);

        List columnas =driver.findElements(By.xpath("//table/thead/tr/th"));
        System.out.println("La Cantidad de columnas de la tabla es: "+ columnas.size());

        List filas = driver.findElements(By.xpath("//table[@class='dataTable']/tbody/tr"));
        System.out.println("La cantidad de filas de la tabla es: " + filas.size());

    }

    @Test
    public void consultarValoresTabla() throws Exception {
        driver.get(URL_TABLAS);

        WebElement tabla = driver.findElement(By.xpath("//table[@class='dataTable']"));

        WebElement fila = tabla.findElement(By.xpath("//tbody/tr[2]"));

        WebElement celda = tabla.findElement(By.xpath("//tbody/tr[2]/td[3]"));

        System.out.println("Los datos de la fila 2 son: "+ fila.getText());
        System.out.println("La información de la celda 3 es:" + celda.getText());

        Utilidades.esperar(15);

    }

    @Test
    public void consultarTodosValoresTabla() throws Exception {
        driver.get(URL_TABLAS);

        WebElement tabla = driver.findElement(By.xpath("//table[@class='dataTable']"));

        List empresas = tabla.findElements(By.xpath("//tbody/tr/td[1]"));

        for (int i = 1; i < empresas.size(); i++) {
            System.out.println("La Empresa #: " + (i) + " - " +
                    tabla.findElement(By.xpath("//tbody/tr["+ i +"]/td[1]")).getText());
        }

        Utilidades.esperar(15);
    }


    @After
    public void close(){
        driver.quit();
    }
}
