@Test
Feature: Gestionar usuarios en la App Orange HRM

  Background: Ingreso a la App
    Given que Yeison quiere acceder a Orange HRM
    When él ingresa el usuario admin y la clave admin123

  @crearusuario
  Scenario Outline: Crear Usuario en la App
    When él diligencia los datos del usuario
      | firstname   | middlename   | lastname   | username   | password   | confirmpassword   |
      | <firstname> | <middlename> | <lastname> | <username> | <password> | <confirmpassword> |
    Then él verifica que usuario existe
      | firstname   | middlename   |
      | <firstname> | <middlename> |
    Examples:
      | firstname | middlename | lastname | username | password  | confirmpassword |
      | Yeison    | Ferney     | Arias    | yfarias17 | Yfarias1* | Yfarias1*       |
