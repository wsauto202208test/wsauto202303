@Regresion
Feature: Registro en App Parabank

  @ScenarioOutline
  Scenario Outline: Succes Register in Parabank
    Given Yeison quiere acceder a Parabank
    When él realiza el registro en la app
      | firstname   | lastname   | address   | city   | state   | zipcode   | phone   | ssn   | user   | pass   | confir   |
      | <firstname> | <lastname> | <address> | <city> | <state> | <zipcode> | <phone> | <ssn> | <user> | <pass> | <confir> |
    Then él verifica el resgitro Exitoso
      | mensaje   | user   |
      | <mensaje> | <user> |
    Examples:
      | firstname | lastname | address   | city        | state     | zipcode | phone         | ssn  | user     | pass     | confir   | mensaje         |
##@externaldata@./src/test/resources/Datadriven/dtDatos.xlsx@Hoja1@1
   |Linea 1   | Arias       | Cl 2 #3 4    | Medellin    | Antioquia    |1234   |3014566770   |2343   | yfarias3    | yfarias*    | yfarias*    | Welcome yfarias |



  @ScenarioOutline
  Scenario Outline: Succes Register in Parabank
    Given Yeison quiere acceder a Parabank
    When él realiza el registro en la app
      | firstname   | lastname   | address   | city   | state   | zipcode   | phone   | ssn   | user   | pass   | confir   |
      | <firstname> | <lastname> | <address> | <city> | <state> | <zipcode> | <phone> | <ssn> | <user> | <pass> | <confir> |
    Then él verifica el usuario existe
      | mensaje   | user   |
      | <mensaje> | <user> |
    Examples:
      | firstname | lastname | address   | city        | state     | zipcode | phone         | ssn  | user     | pass     | confir   | mensaje         |
      | Yeison    | Arias    | Cl 2 #3 4 | Medellin    | Antioquia | 1234    | 3014566770    | 2343 | yfarias3 | yfarias* | yfarias* | Usuario Existe |
