@Regresion
Feature: Login in Mestis App

  Background: Abrir la Url de Metis
    Given Yeison quiere acceder a Metis

  @ScenarioLogin
  Scenario: Succes Login in Mestis
    When él ingresas el usuario demo y la clave demo
    Then él verifica el mensaje Bootstrap-Admin-Template

  @ScenarioLoginDataTable
  Scenario: Succes Login in Mestis
    When él ingresa las credenciales
    |user   |pass     |logout|
    |demo   |demo     |S     |
    |yeison |password |N     |
    Then él verifica el mensaje Bootstrap-Admin-Template

  @ScenarioOutline
  Scenario Outline: Succes Login in Mestis
    When él ingresa las credenciales de acceso
      |user     |pass       |
      |<user>   |<pass>     |
    Then él verifica el mensaje
      |mensaje                        |
      |<mensaje>                      |
    Examples:
      |user   |pass     |mensaje                      |
      |demo   |demo     |Bootstrap-Admin-Template     |
      |yeison |password |Bootstrap-Admin-Template     |
      |andres |password |Bootstrap-Admin-Template     |