package co.com.test.definitions;

import co.com.test.models.DataLogin;
import co.com.test.steps.LoginMetisSteps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class LoginMetisDefinition {
    @Steps
    LoginMetisSteps loginMetisSteps;

    @Given("^Yeison quiere acceder a Metis$")
    public void yeisonQuiereAccederAMetis() {
        loginMetisSteps.abrirPaginaMetis();
    }

    @When("^él ingresas el usuario (.*) y la clave (.*)$")
    public void élIngresasLasCredenciales(String username, String password) {
        loginMetisSteps.autenticarseEnLaPagina(username, password);
    }

    @Then("^él verifica el mensaje (.*)$")
    public void élVerificaElMensajeBootstrapAdminTemplate(String mensaje) {
        loginMetisSteps.verificarAutenticacionExitosa(mensaje);
    }

    @When("^él ingresa las credenciales$")
    public void élIngresaLasCredenciales(DataTable datos) {
        List<List<String>> data = datos.asLists(String.class);
        for (int i = 1; i < data.size(); i++) {
            loginMetisSteps.autenticarseMetis(data.get(i));
        }

    }

    @When("^él ingresa las credenciales de acceso$")
    public void élIngresaLasCredencialesDeAcceso(List<DataLogin> datosLogin) {
        loginMetisSteps.credencialesDataLogin(datosLogin.get(0));
    }

    @Then("^él verifica el mensaje$")
    public void élVerificaElMensaje(List<DataLogin> mensaje) {
        loginMetisSteps.verificarAutenticacionExitosa(mensaje.get(0).getMensaje());
    }

}
