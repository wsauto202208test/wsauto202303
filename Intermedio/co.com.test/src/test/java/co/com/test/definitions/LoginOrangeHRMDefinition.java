package co.com.test.definitions;

import co.com.test.steps.LoginOrangeHRMSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginOrangeHRMDefinition {
    @Steps
    LoginOrangeHRMSteps loginOrangeHRMSteps;

    @Given("^que Yeison quiere acceder a Orange HRM$")
    public void queYeisonQuiereAccederAOrangeHRM() {
        loginOrangeHRMSteps.abrirLaPagina();
    }

    @When("^él ingresa el usuario (.*) y la clave (.*)$")
    public void élIngresaElUsuarioAdminYLaClaveAdmin(String usuario, String clave) {
        loginOrangeHRMSteps.ingresarCredenciales(usuario, clave);
    }

    @Then("^él verifica (.*) en la pantalla Principal$")
    public void élVerificaElMensajeEnPantalla(String mensaje) {
        loginOrangeHRMSteps.verificarNombreUsuario(mensaje);
    }
}
