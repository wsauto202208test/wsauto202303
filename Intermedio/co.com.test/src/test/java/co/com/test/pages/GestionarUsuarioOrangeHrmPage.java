package co.com.test.pages;

import co.com.test.models.RegistroUsuarioData;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static co.com.test.utilidades.Constantes.ESPERA;
import static co.com.test.utilidades.Constantes.TABLA_XPATH;
import static co.com.test.utilidades.Utilidades.esperar;
import static co.com.test.utilidades.Utilidades.scrollToObjectXpath;
import static net.serenitybdd.core.Serenity.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.hamcrest.MatcherAssert.*;

public class GestionarUsuarioOrangeHrmPage extends PageObject {
    @FindBy(xpath = "//span[text()='PIM']")
    WebElementFacade MENU_PIM;
    @FindBy(xpath = "//div[@class='orangehrm-paper-container']/div/button")
    WebElementFacade ADICIONAR_EMPLEADO;

    @FindBy(name = "firstName")
    WebElementFacade FIRST_NAME;
    @FindBy(name = "middleName")
    WebElementFacade MIDDLE_NAME;
    @FindBy(name = "lastName")
    WebElementFacade LAST_NAME;
    @FindBy(xpath = "//span[@class='oxd-switch-input oxd-switch-input--active --label-right']")
    WebElementFacade CREATE_LOGIN_DETAILS;
    @FindBy(xpath = "(//div[@class='oxd-input-group oxd-input-field-bottom-space']/div/input[@class='oxd-input oxd-input--active'])[2]")
    WebElementFacade USER_NAME;
    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div/form/div[1]/div[2]/div[4]/div/div[1]/div/div[2]/input")
    WebElementFacade PASSWORD;
    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div/form/div[1]/div[2]/div[4]/div/div[2]/div/div[2]/input")
    WebElementFacade CONFIRM_PASSWORD;
    @FindBy(xpath = "//div[@class='oxd-form-actions']/button[@type='submit']")
    WebElementFacade SAVE;
    @FindBy(xpath = "//*[@id='app']/div[1]/div[2]/div[2]/div/div/form/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/input")
    WebElementFacade EMPLOYEE_ID;

    @FindBy(xpath = "//*[@id='app']/div[1]/div[2]/div[2]/div/div[1]/div[2]/form/div[1]/div/div[2]/div/div[2]/input")
    WebElementFacade SEARCH_EMPLOYEE_ID;
    @FindBy(xpath = "//button[@type='submit']")
    WebElementFacade SEARCH;
    @FindBy(xpath = "//*[@id='app']/div[1]/div[2]/div[2]/div/div[1]/div[2]/form/div[1]/div/div[1]/div/div[2]/div/div/input")
    WebElementFacade NAME_SEARCH;
    @FindBy(xpath = "//div[@role='table' and @class='oxd-table orangehrm-employee-list']")
    WebElement TABLA_RESULTADOS;


    public void accederMenuPIM() {
        MENU_PIM.click();
    }

    public void diligenciarDatosUsuario(RegistroUsuarioData registroUsuarioData) throws InterruptedException {
        ADICIONAR_EMPLEADO.click();
        FIRST_NAME.sendKeys(registroUsuarioData.getFirstname());
        MIDDLE_NAME.sendKeys(registroUsuarioData.getMiddlename());
        LAST_NAME.sendKeys(registroUsuarioData.getLastname());
        CREATE_LOGIN_DETAILS.click();
        USER_NAME.sendKeys(registroUsuarioData.getUsername());
        PASSWORD.type(registroUsuarioData.getPassword());
        CONFIRM_PASSWORD.sendKeys(registroUsuarioData.getConfirmpassword());
        setSessionVariable("employeeid").to(EMPLOYEE_ID.getValue());
        SAVE.click();
    }

    public void verificarUsuarioCreado(RegistroUsuarioData registroUsuarioData) throws InterruptedException {
        accederMenuPIM();
        NAME_SEARCH.sendKeys(registroUsuarioData.getFirstname());
        SEARCH_EMPLOYEE_ID.sendKeys(sessionVariableCalled("employeeid").toString());
        WaitUntil.the(TABLA_XPATH,isVisible()).forNoMoreThan(ESPERA).seconds();
        esperar(3);
        SEARCH.click();
        System.out.println("Hola");
        WaitUntil.the("//div[@role='table' and @class='oxd-table orangehrm-employee-list']/div[@class='oxd-table-body']",isVisible()).forNoMoreThan(15).seconds();
        esperar(3);
        WebElement fila = TABLA_RESULTADOS.findElement(By.xpath("//div[@class='oxd-table-body']"));
        List<WebElement> celda = TABLA_RESULTADOS.findElements(By.xpath("//div[@role='table' and @class='oxd-table orangehrm-employee-list']//div[@class='oxd-table-body']//div[@role='cell']"));
        for (int i = 1; i < celda.size(); i++) {
            System.out.println("Datos Celda "+ i +": "+ celda.get(i).getText());
        }
        scrollToObjectXpath("//div[@role='table' and @class='oxd-table orangehrm-employee-list']", getWebdriverManager().getCurrentDriver());
        assertThat(sessionVariableCalled("employeeid").toString(),containsText(celda.get(1).getText()));
        //}
    }
}
