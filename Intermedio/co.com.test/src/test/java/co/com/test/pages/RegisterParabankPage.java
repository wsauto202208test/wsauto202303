package co.com.test.pages;

import co.com.test.models.DataParabankRegister;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

import static co.com.test.utilidades.Constantes.URL_PARABANK_PAGE;
import static org.hamcrest.MatcherAssert.assertThat;


@DefaultUrl(URL_PARABANK_PAGE)
public class RegisterParabankPage extends PageObject {
    @FindBy(id="customer.firstName")
    WebElementFacade FIRST_NAME;
    @FindBy(id="customer.lastName")
    WebElementFacade LAST_NAME;
    @FindBy(id="customer.address.street")
    WebElementFacade ADDRESS;
    @FindBy(id="customer.address.city")
    WebElementFacade CITY;
    @FindBy(id="customer.address.state")
    WebElementFacade STATE;
    @FindBy(id="customer.address.zipCode")
    WebElementFacade ZIP_CODE;
    @FindBy(id="customer.phoneNumber")
    WebElementFacade PHONE_NUMBER;
    @FindBy(id="customer.ssn")
    WebElementFacade SSN;
    @FindBy(id="customer.username")
    WebElementFacade USER_NAME;
    @FindBy(id="customer.password")
    WebElementFacade PASSWORD;
    @FindBy(id="repeatedPassword")
    WebElementFacade CONFIRM_PASSWORD;
    @FindBy(xpath="//input[@value='Register']")
    WebElementFacade REGISTER;
    @FindBy(xpath="//div[@id='rightPanel']")
    WebElementFacade MENSAJE;

    @FindBy(id="customer.username.errors")
    WebElementFacade USUARIO_EXISTE;


    public void diligenciarFormulario(DataParabankRegister dataParabankRegister) {
        FIRST_NAME.sendKeys(dataParabankRegister.getFirstname());
        LAST_NAME.sendKeys(dataParabankRegister.getLastname());
        ADDRESS.sendKeys(dataParabankRegister.getAddress());
        CITY.sendKeys(dataParabankRegister.getCity());
        STATE.sendKeys(dataParabankRegister.getState());
        ZIP_CODE.sendKeys(dataParabankRegister.getZipcode());
        PHONE_NUMBER.sendKeys(dataParabankRegister.getPhone());
        SSN.sendKeys(dataParabankRegister.getSsn());
        USER_NAME.sendKeys(dataParabankRegister.getUser());
        PASSWORD.sendKeys(dataParabankRegister.getPass());
        CONFIRM_PASSWORD.sendKeys(dataParabankRegister.getConfir());
        REGISTER.click();
    }

    public void verificarMensajeExitoso(DataParabankRegister dataParabankRegister) {
        assertThat(MENSAJE.getText(), containsText(dataParabankRegister.getMensaje()));
    }

    public void elMensajeUsuarioExiste(DataParabankRegister conLosdDatos) {
        assertThat(MENSAJE.getText(), containsText(conLosdDatos.getMensaje()));
    }
}
