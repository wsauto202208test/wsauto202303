package co.com.test.pages;

import co.com.test.models.DataParabankRegister;
import co.com.test.utilidades.Constantes;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.support.FindBy;

import static co.com.test.utilidades.Constantes.*;

@DefaultUrl(URL_PARABANK_PAGE)
public class Revisar extends RegisterParabankPage {
    @FindBy(id="customer.username.errors")
    WebElementFacade USUARIO_EXISTE;
    public void elMensade(DataParabankRegister conLosdDatos) {
        diligenciarFormulario(conLosdDatos);
        MatcherAssert.assertThat(USUARIO_EXISTE.getText(), equals(conLosdDatos.getMensaje()));
    }
}
