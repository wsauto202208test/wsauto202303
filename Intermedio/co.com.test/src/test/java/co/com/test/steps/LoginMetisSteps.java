package co.com.test.steps;

import co.com.test.models.DataLogin;
import co.com.test.pages.LoginMetisPage;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class LoginMetisSteps {
    LoginMetisPage loginMetisPage;

    @Step
    public void abrirPaginaMetis() {
        loginMetisPage.open();
    }

    @Step
    public void autenticarseEnLaPagina(String username, String password) {
        loginMetisPage.ingresarCredencialesDeAcceso(username, password);
    }

    @Step
    public void verificarAutenticacionExitosa(String mensaje) {
        loginMetisPage.verificarAutenticacionExitosa(mensaje);
    }

    @Step
    public void autenticarseMetis(List<String> data) {
        loginMetisPage.autenticarseMetis(data);
    }

    @Step
    public void credencialesDataLogin(DataLogin datosLogin) {
        loginMetisPage.credencialesDataLogin(datosLogin);
    }
}
