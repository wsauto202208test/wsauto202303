package co.com.test.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.openqa.selenium.support.FindBy;

import static org.hamcrest.MatcherAssert.*;

@DefaultUrl("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login")
public class LoginOrangeHRMPage extends PageObject {
    @FindBy (name = "username")
    WebElementFacade USERNAME;
    @FindBy (name = "password")
    WebElementFacade PASSWORD;
    @FindBy (xpath = "//button[@type='submit']")
    WebElementFacade LOGIN;

    @FindBy (xpath = "//h6[@class='oxd-text oxd-text--h6 oxd-topbar-header-breadcrumb-module']")
    WebElementFacade MENSAJE_PANTALLA;

    public void ingresarCredenciales(String usuario, String clave) {
        USERNAME.sendKeys(usuario);
        PASSWORD.sendKeys(clave);
        LOGIN.click();
    }

    public void verificarNombreUsuario(String mensaje) {
        assertThat(MENSAJE_PANTALLA.getText(),containsText(mensaje));
    }
}
