package co.com.test.definitions;

import co.com.test.models.RegistroUsuarioData;
import co.com.test.steps.GestionarUsuarioOrangeHrmSteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class GestionarUsuarioOrangeHrm {
    @Steps
    GestionarUsuarioOrangeHrmSteps gestionarUsuarioOrangeHrmSteps;

    @When("^él diligencia los datos del usuario$")
    public void élDiligenciaLosDatosDelUsuario(List<RegistroUsuarioData> registroUsuarioData) throws InterruptedException {
        gestionarUsuarioOrangeHrmSteps.ingresarDatosUsuario(registroUsuarioData.get(0));
    }

    @Then("^él verifica que usuario existe$")
    public void élVerificaQueUsuarioExiste(List<RegistroUsuarioData> registroUsuarioData) throws InterruptedException {
        gestionarUsuarioOrangeHrmSteps.verificarUsuarioCreado(registroUsuarioData.get(0));
    }
}
