package co.com.test.steps;

import co.com.test.pages.LoginOrangeHRMPage;
import net.thucydides.core.annotations.Step;

public class LoginOrangeHRMSteps {
    LoginOrangeHRMPage loginOrangeHRMPage;

    @Step
    public void abrirLaPagina() {
        loginOrangeHRMPage.open();
    }

    @Step
    public void ingresarCredenciales(String usuario, String clave) {
        loginOrangeHRMPage.ingresarCredenciales(usuario, clave);
    }

    @Step
    public void verificarNombreUsuario(String mensaje) {
        loginOrangeHRMPage.verificarNombreUsuario(mensaje);
    }
}
