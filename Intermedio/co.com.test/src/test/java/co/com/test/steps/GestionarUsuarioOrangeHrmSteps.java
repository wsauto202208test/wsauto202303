package co.com.test.steps;

import co.com.test.models.RegistroUsuarioData;
import co.com.test.pages.GestionarUsuarioOrangeHrmPage;
import net.thucydides.core.annotations.Step;

public class GestionarUsuarioOrangeHrmSteps {
    GestionarUsuarioOrangeHrmPage gestionarUsuarioOrangeHrmPage;

    @Step
    public void ingresarDatosUsuario(RegistroUsuarioData registroUsuarioData) throws InterruptedException {
        gestionarUsuarioOrangeHrmPage.accederMenuPIM();
        gestionarUsuarioOrangeHrmPage.diligenciarDatosUsuario(registroUsuarioData);
    }

    @Step
    public void verificarUsuarioCreado(RegistroUsuarioData registroUsuarioData) throws InterruptedException {
        gestionarUsuarioOrangeHrmPage.accederMenuPIM();
        gestionarUsuarioOrangeHrmPage.verificarUsuarioCreado(registroUsuarioData);
    }
}
