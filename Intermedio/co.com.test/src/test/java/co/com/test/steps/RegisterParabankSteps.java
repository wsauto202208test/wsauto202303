package co.com.test.steps;

import co.com.test.models.DataParabankRegister;
import co.com.test.pages.RegisterParabankPage;
import net.thucydides.core.annotations.Step;

public class RegisterParabankSteps {
    RegisterParabankPage registerParabankPage;

    @Step
    public void abrirPagina() {
        registerParabankPage.open();
    }

    @Step
    public void registrarDatos(DataParabankRegister dataParabankRegister) {
        registerParabankPage.diligenciarFormulario(dataParabankRegister);
    }

    @Step
    public void verificarMensajeExitoso(DataParabankRegister dataParabankRegister) {
        registerParabankPage.verificarMensajeExitoso(dataParabankRegister);
    }

    @Step
    public void elMensajeDeQueElUsuarioExiste(DataParabankRegister dataParabankRegister) {
        registerParabankPage.elMensajeUsuarioExiste(dataParabankRegister);
    }
}
