package co.com.test.pages;

import co.com.test.models.DataLogin;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static co.com.test.utilidades.Constantes.*;
import static org.hamcrest.MatcherAssert.assertThat;

@DefaultUrl(URL_COLORLIB)
public class LoginMetisPage extends PageObject {
    @FindBy(xpath = "/html/body/div/div/div/form/input[@placeholder='Username']")
    WebElementFacade txtUserName;
    @FindBy(xpath = "/html/body/div/div/div/form/input[@placeholder='Password']")
    WebElementFacade txtPassword;
    @FindBy(xpath = "/html/body/div/div/div/form/button[contains(text(),'Sign in')]")
    WebElementFacade btnSignIn;

    @FindBy(id = "bootstrap-admin-template")
    WebElementFacade LblMensajePantalla;

    @FindBy(xpath = "/html/body/div[1]/div[1]/nav/div/div[1]/div[3]/a")
    WebElementFacade btnLogout;

    public void ingresarCredencialesDeAcceso(String username, String password) {
        txtUserName.sendKeys(username);
        txtPassword.sendKeys(password);
        btnSignIn.click();

    }

    public void verificarAutenticacionExitosa(String mensaje) {
        assertThat(LblMensajePantalla.getText(), containsText(mensaje));
    }

    public void autenticarseMetis(List<String> data) {
        txtUserName.sendKeys(data.get(USER_NAME));
        txtPassword.sendKeys(data.get(PASSWORD));
        btnSignIn.click();
        if (data.get(LOGOUT).equals("S")) btnLogout.click();
    }

    public void credencialesDataLogin(DataLogin datosLogin) {
        txtUserName.sendKeys(datosLogin.getUser());
        txtPassword.sendKeys(datosLogin.getPass());
        btnSignIn.click();
    }
}
