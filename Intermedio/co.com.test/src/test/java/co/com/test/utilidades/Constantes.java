package co.com.test.utilidades;

public class Constantes {
    public static final String URL_COLORLIB = "https://colorlib.com/polygon/metis/login.html";
    public static final String URL_PARABANK_PAGE = "https://parabank.parasoft.com/parabank/register.htm";
    public static final String TABLA_XPATH = "//div[@role='table' and @class='oxd-table orangehrm-employee-list']";
    public static final int ESPERA = 15;
    public static final int USER_NAME = 0;
    public static final int PASSWORD = 1;
    public static final int LOGOUT = 2;
}
