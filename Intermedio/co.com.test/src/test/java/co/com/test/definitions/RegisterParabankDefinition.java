package co.com.test.definitions;

import co.com.test.models.DataParabankRegister;
import co.com.test.steps.RegisterParabankSteps;
import co.com.test.steps.Verificar;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class RegisterParabankDefinition {
    @Steps
    RegisterParabankSteps registerParabankSteps;

    @Given("^Yeison quiere acceder a Parabank$")
    public void yeisonQuiereAccederAParabank() {
        registerParabankSteps.abrirPagina();
    }

    @When("^él realiza el registro en la app$")
    public void élRealizaElRegistroEnLaApp(List<DataParabankRegister> dataParabankRegisters) {
        registerParabankSteps.registrarDatos(dataParabankRegisters.get(0));
    }

    @Then("^él verifica el resgitro Exitoso$")
    public void élVerificaElResgitroExitoso(List<DataParabankRegister> dataParabankRegisters) {
        registerParabankSteps.verificarMensajeExitoso(dataParabankRegisters.get(0));
    }

    @Then("^él verifica el usuario existe$")
    public void élVerificaElUsuarioExiste(List<DataParabankRegister> conLosdDatos) {
        registerParabankSteps.elMensajeDeQueElUsuarioExiste(conLosdDatos.get(0));
    }
}
