package co.com.test.utilidades;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Utilidades {
    public static void esperar(long tiempo) throws InterruptedException {
        tiempo = tiempo * 1000;
        Thread.sleep(tiempo);
    }

    public static void scrollToObjectXpath(String locator, WebDriver driver) {
        WebElement element = driver.findElement(By.xpath(locator));
        Actions action = new Actions(driver);
        action.moveToElement(element);
        action.perform();
    }
}
