package encapsulamiento.paquete1;
//Encapsulamiento y Metodos Setter y Getter (Métodos Accesores)
public class Ej10_Clase1 {
    private int edad;
    private String primerNombre;

    //Metodo Setter con el cual establecemos la edad
    public void setEdad(int edad){
        this.edad = edad;
    }

    //Metodo Getter con el cual se imprime la edad
    public int getEdad(){
        return edad;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }
}
