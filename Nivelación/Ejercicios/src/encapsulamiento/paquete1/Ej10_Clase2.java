package encapsulamiento.paquete1;

public class Ej10_Clase2 {
    public static void main(String[] args) {
        Ej10_Clase1 objeto1=new Ej10_Clase1();

        objeto1.setEdad(20);
        objeto1.setPrimerNombre("Yeison Arias");
        System.out.println("La edad de "+ objeto1.getPrimerNombre() +" es " + objeto1.getEdad());
    }
}
