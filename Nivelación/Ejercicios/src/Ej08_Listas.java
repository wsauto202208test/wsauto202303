import java.util.ArrayList;
import java.util.List;

public class Ej08_Listas {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<String>();
        lista.add("Venezuela");
        lista.add("Colombia");
        lista.add("Panama");
        lista.add("Costa Rica");
        lista.add("Honduras");

        lista.clear();
        if (lista.isEmpty()) {
            System.out.println("La lista no contiene ningun elemento");
        }else {
            System.out.println("El tamaño de la lista es: "+lista.size());
        }


    }
}
