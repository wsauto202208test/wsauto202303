public class Ej02_IdentificacionGeneracion {
    /*Edad entre 0-10 años  -- niño
    * Edad entre 11-14 años -- preadolescente
    * Edad entre 15-18 años -- adolescente
    * Edad entre 19-25 años -- Joven
    * Edad entre 26-65 años -- Adulto
    * Mayo a 65 año         -- Anciano
    * */
    public static void main(String[] args) {
        Integer edad=65;

        if (edad>=0 && edad<=10) {
            System.out.println("La edad " + edad + " corresponde a un niño");
        }else if (edad>=11 && edad<=14){
            System.out.println("La edad " + edad + " corresponde a un preadolescente");
        } else if (edad>=15 && edad<=18) {
            System.out.println("La edad " + edad + " corresponde a un adolescente");
        } else if (edad>=19 && edad<=25) {
            System.out.println("La edad " + edad + " corresponde a un Joven");
        } else if (edad>=25 && edad<=65) {
            System.out.println("La edad " + edad + " corresponde a un adulto");
        }else{
            System.out.println("La edad " + edad + " corresponde a un anciano");
        }

    }
}
