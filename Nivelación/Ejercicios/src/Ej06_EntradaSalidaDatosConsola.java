import java.util.Scanner;

public class Ej06_EntradaSalidaDatosConsola {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

       double numero;
        System.out.println("Ingrese un número: ");
        numero = teclado.nextDouble();
        System.out.println("El numero ingresado es: "+numero);

        int numeroEntero;
        System.out.println("Ingrese un número: ");
        numeroEntero = teclado.nextInt();
        System.out.println("El numero ingresado es: "+numeroEntero);

        float numeroFloat;
        System.out.println("Ingrese un número: ");
        numeroFloat = teclado.nextFloat();
        System.out.println("El numero ingresado es: "+numeroFloat);

        String cadena;
        System.out.print("Ingrese un cadena: ");
        cadena = teclado.nextLine();
        System.out.println("La cadena es: "+cadena);

        char unaLetra;
        System.out.print("Ingrese un caracter: ");
        unaLetra = teclado.next().charAt(1);
        System.out.println("La cadena es: "+unaLetra);
    }
}
