import java.util.ArrayList;
import java.util.List;

public class Ej08_ListasRemove {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<String>();
        lista.add(0,"Venezuela");
        lista.add(1,"Colombia");
        lista.add(2,"Panama");
        lista.add(3,"Costa Rica");
        lista.add(4,"Honduras");
        int eliminar = Integer.parseInt(String.valueOf((lista.indexOf("Panama"))));

        for (int i = 0; i < lista.size(); i++) {
            if (i == eliminar) {
                lista.remove(i);
            }
            System.out.println(lista.get(i));
        }

    }
}
