package metodos_no_static;

import javax.swing.*;

public class PruebasOperacion {

    public static void main(String[] args) {
        Operacion op1= new Operacion();
        int numero1 = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero: "));
        int numero2 = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero: "));

        System.out.println("El resultado de la sumar es: "+op1.sumar(numero1,numero2));
        System.out.println("El resultado de la sumar es: "+op1.restar(numero1,numero2));

    }
}
