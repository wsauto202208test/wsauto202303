package metodos_no_static;

public class Operacion {
    public int sumar(int numero1, int numero2){
        int resultado= numero1+numero2;
        return resultado;
    }

    public int restar(int numero1, int numero2){
        int resultado= numero1-numero2;
        return resultado;
    }
}
