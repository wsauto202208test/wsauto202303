package herencia;

public class Estudiante extends Persona{
    private int codigoEstudiante;
    private float notaFinal;

    //Constructor de Estudiante
    public Estudiante(String nombre, String apellidos, int edad,int codigoEstudiante, float notaFinal){
        super(nombre,apellidos,edad);
        this.codigoEstudiante = codigoEstudiante;
        this.notaFinal = notaFinal;
    }

    public void mostrarDatos(){
        System.out.println("Nombres:"+getNombre());
        System.out.println("Apellidos:"+getApellidos());
        System.out.println("Edad:"+getEdad());
        System.out.println("Codigo del Estudiante:"+codigoEstudiante);
        System.out.println("La nota final:"+notaFinal);
    }
}
