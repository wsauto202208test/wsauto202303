package parametrosyargumentos;

public class Operaciones {
    //Atributos
    int suma;
    int resta;
    int multiplicacion;
    int division;

    //Metodo para sumar 2 valores
    public void sumar(int a, int b){
        suma = a + b;
    }

    //Metodo para restar 2 valores
    public void restar(int a, int b){
        resta = a - b;
    }

    //Metodo para multiplicar 2 valores
    public void multiplicar(int a, int b){
        multiplicacion = a * b;
    }


    //Metodo para dividir 2 valores
     public void dividir(int a, int b){
        division = a / b;
    }

    public void mostrar(){
        System.out.println("La suma es: " + suma);
        System.out.println("La resta es: " + resta);
        System.out.println("La multiplicacion es: " + multiplicacion);
        System.out.println("La division es: " + division);
    }

}
