import java.util.Scanner;

public class Ej09_TryCatch {
    public static void main(String[] args) {
        int dividendo;
        int divisor;
        int resultado = 0;

        Scanner teclado = new Scanner(System.in);
        System.out.print("Ingrese un valor para el dividendo: ");
        dividendo = teclado.nextInt();

        System.out.print("Ingrese un valor para el divisor: ");
        divisor = teclado.nextInt();

        try {
            System.out.println("MENSAJE ANTES DE LA OPERACIÓN");
            resultado = dividendo / divisor ;
            System.out.println("El resultado de la división es: " + resultado);
        }catch (ArithmeticException ex){
            System.out.println(ex.getMessage());
        }finally {
            System.out.println("MENSAJE DESPUES DE LA OPERACIÓN");
        }
    }
}
