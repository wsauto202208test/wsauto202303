import javax.swing.*;

//Integer edad2 = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad"));
public class Ej04_LeerDatosTeclado {
    public static void main(String[] args) {
        Integer edad;
        //Variable encargada de almacenar los valores que se ingresan por teclado

        //Asignación del valor ingresado por teclado a la variable edad
        edad = Integer.parseInt(JOptionPane.showInputDialog("Digite la edad de la persona: "));

        if (edad>=0 && edad<=10) {
            System.out.println("La edad " + edad + " corresponde a un niño");
        }else if (edad>=11 && edad<=14){
            System.out.println("La edad " + edad + " corresponde a un preadolescente");
        } else if (edad>=15 && edad<=18) {
            System.out.println("La edad " + edad + " corresponde a un adolescente");
        } else if (edad>=19 && edad<=25) {
            System.out.println("La edad " + edad + " corresponde a un Joven");
        } else if (edad>=25 && edad<=65) {
            System.out.println("La edad " + edad + " corresponde a un adulto");
        }else if(edad>65) {
            System.out.println("La edad " + edad + " corresponde a un anciano");
        }else{
            System.out.println("La edad ingresada no es válida");
        }
    }
}
