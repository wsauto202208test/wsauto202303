import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Ej08_ListasWhile {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<String>();
        lista.add(0,"Venezuela");
        lista.add(1,"Colombia");
        lista.add(2,"Panama");
        lista.add(3,"Costa Rica");
        lista.add(4,"Honduras");
        Iterator<String> listaIterable = lista.iterator();

        while (listaIterable.hasNext()){
            System.out.println(listaIterable.next());
        }

    }
}
