package clase_metodos_abstract.ej1;

public class Planta extends SerVivo{

    @Override
    public void alimentarse() {
        System.out.println("La planta se alimenta a traves de las raices");
    }
}
