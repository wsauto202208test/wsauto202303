package clase_metodos_abstract.ej1;

public class Principal {
    public static void main(String[] args) {
        Planta planta = new Planta();
        AnimalCarnivoro animalc= new AnimalCarnivoro();

        planta.alimentarse();
        animalc.alimentarse();
    }
}
