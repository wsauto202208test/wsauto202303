package clase_metodos_abstract.ej1;

public class AnimalHervivoro extends Animal{

    @Override
    public void alimentarse() {
        System.out.println("El animal se alimentar de hierva");
    }
}
