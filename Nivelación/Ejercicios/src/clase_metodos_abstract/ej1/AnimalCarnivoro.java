package clase_metodos_abstract.ej1;

public class AnimalCarnivoro extends Animal{

    @Override
    public void alimentarse() {
        System.out.println("El animal Carnivoro come carne");
    }
}
