package clase_metodos_abstract.ej2;

public class AnimalCarnivoro extends Animal{

    public void alimentarse(){
        System.out.println("El animal Carnivoro se alimenta de Carne");
    }
}
