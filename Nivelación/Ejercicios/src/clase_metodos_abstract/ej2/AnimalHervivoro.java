package clase_metodos_abstract.ej2;

public class AnimalHervivoro extends Animal{

    public void alimentarse(){
        System.out.println("El animal Hervivoro se alimenta de Hierva");
    }
}
