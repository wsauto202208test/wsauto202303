package clase_metodos_abstract.ej2;

public class Principal {
    public static void main(String[] args) {
        Planta planta = new Planta();
        AnimalCarnivoro animalCarnivoro = new AnimalCarnivoro();

        planta.alimentarse();
        animalCarnivoro.alimentarse();
    }
}
