//Miembros estaticos de una clase
package static_miembros;

public class Estatico {
    private static String mensaje= "Primer mensaje";

    public static int sumar(int n1, int n2){
        int sum = n1 + n2;
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(Estatico.mensaje);
        System.out.println("La suma tiene como resultado: "+Estatico.sumar(3,4));
    }
}
