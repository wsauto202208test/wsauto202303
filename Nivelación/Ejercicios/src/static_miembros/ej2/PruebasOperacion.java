package static_miembros.ej2;

import javax.swing.*;

import static static_miembros.ej2.Operacion.restar;
import static static_miembros.ej2.Operacion.sumar;


public class PruebasOperacion {
    public static void main(String[] args) {
        int numero1 = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero: "));
        int numero2 = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero: "));

        System.out.println("La operación sumar da como resultado: " + sumar(numero1, numero2));
        System.out.println("La operación restar da como resultado: " + restar(numero1, numero2));
    }
}
