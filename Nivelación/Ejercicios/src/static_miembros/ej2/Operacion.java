package static_miembros.ej2;

public class Operacion {
    public static int sumar(int num1, int num2){
        int resultado = num1 + num2;
        return resultado;
    }

    public static int restar(int num1, int num2){
        int resultado = num1 - num2;
        return resultado;
    }
}
