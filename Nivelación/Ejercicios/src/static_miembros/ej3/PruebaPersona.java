package static_miembros.ej3;

import static_miembros.ej2.Operacion;

public class PruebaPersona {
    public static void main(String[] args) {
        Persona persona1 = new Persona("Juan", 33);
        Persona persona2 = new Persona("Ana", 50);
        persona1.imprimir();
        persona2.imprimir();
        Persona personaMayor = persona1.mayor(persona1,persona2);
        System.out.println("Persona con mayor edad");
        personaMayor.imprimir();
        System.out.println(Operacion.restar(2,3));

    }
}
