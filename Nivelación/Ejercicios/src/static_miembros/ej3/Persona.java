package static_miembros.ej3;

public class Persona {
    private String nombre;
    private int edad;

    public Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public void imprimir(){
        System.out.println("La Persona: " + nombre+" tiene "+ edad+" años");
    }

    public static Persona mayor(Persona per1, Persona per2) {
        if (per1.edad >= per2.edad)
            return per1;
        else
            return per2;
    }

}
