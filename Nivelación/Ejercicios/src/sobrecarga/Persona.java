package sobrecarga;

public class Persona {
    //Atributos
    String nombre;
    int edad;
    int cedula;

    //Metodos

    //Metodo constructor
    public Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public Persona(int cedula) {
        this.cedula = cedula;
    }

    public void correr(){
        System.out.println("Yo "+nombre+" tengo "+edad+" años y estoy corriendo una carrera");
    }

    public void correr(int km){
        System.out.println("He recorrido "+km+" kilometros");
    }
}
