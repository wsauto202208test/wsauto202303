import javax.swing.*;

public class Ej06_LeerDatosPopUp {
    public static void main(String[] args) {
        int numero;
        numero = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero entero: "));

        System.out.println("El numero digitado es: "+numero);
    }
}
