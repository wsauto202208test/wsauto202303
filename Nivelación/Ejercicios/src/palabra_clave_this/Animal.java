package palabra_clave_this;

public class Animal {
    private String nombre;
    private float peso;
    private String raza;

    public Animal(String _nombre, float _peso, String _raza) {
        nombre = _nombre;
        peso = _peso;
        raza = _raza;
    }

    public static void main(String[] args) {
        Animal animal1 = new Animal("Firulais",20,"Doberman");
        System.out.println(animal1.nombre);
        System.out.println(animal1.peso);
        System.out.println(animal1.raza);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }
}
