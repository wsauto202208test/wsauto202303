package polimorfismo;

public class Principal {
    public static void main(String[] args) {
        Vehiculo misVehiculos[] = new Vehiculo[4];

        misVehiculos[0] = new Vehiculo("AAA123","Renault","2021");
        misVehiculos[1] = new VehiculoTurismo("BBB123","BMW","2023",5);
        misVehiculos[2] = new VehiculoDeportivo("CCC123","Mercedes","2019",2000);
        misVehiculos[3] = new VehiculoFurgoneta("DDD123","Ferrari","2020",300);

        for(Vehiculo vehiculos:misVehiculos){
            System.out.println(vehiculos.mostrarDatos());
            System.out.println("--------------------------------");
        }

    }
}
